﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class Common_Post
    {
        public static string Post(string postType, string Url, string ReqJson, string actionType, string agentId, string clientRefId, string actionPostType, bool issave = false)
        {
            string ReturnValue = string.Empty;

            if (issave)
            {
                SMBPDataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, "", actionType, agentId, clientRefId, actionPostType);
            }

            StringBuilder sbResult = new StringBuilder();
            HttpWebRequest Http = (HttpWebRequest)WebRequest.Create(Url);

            try
            {
                Http.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");

                Http.Method = postType;
                byte[] lbPostBuffer = Encoding.UTF8.GetBytes(ReqJson);
                Http.ContentLength = lbPostBuffer.Length;
                Http.Timeout = 130000; //5000 milliseconds == 5 seconds// 900000 milliseconds == 900 seconds- 15 mints
                Http.ContentType = "application/json";
                Http.Accept = "application/json";

                using (Stream PostStream = Http.GetRequestStream())
                {
                    PostStream.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                }

                using (HttpWebResponse WebResponse = (HttpWebResponse)Http.GetResponse())
                {
                    if (WebResponse.StatusCode != HttpStatusCode.OK)
                    {
                        string message = String.Format("POST failed. Received HTTP {0}", WebResponse.StatusCode);
                        throw new ApplicationException(message);
                    }
                    else
                    {
                        Stream responseStream = WebResponse.GetResponseStream();
                        if ((WebResponse.ContentEncoding.ToLower().Contains("gzip")))
                        {
                            responseStream = new GZipStream(responseStream, CompressionMode.Decompress);
                        }
                        else if ((WebResponse.ContentEncoding.ToLower().Contains("deflate")))
                        {
                            responseStream = new DeflateStream(responseStream, CompressionMode.Decompress);
                        }
                        StreamReader reader = new StreamReader(responseStream, Encoding.Default);
                        sbResult.Append(reader.ReadToEnd());
                        ReturnValue = sbResult.ToString();
                        responseStream.Close();
                    }
                }
            }
            catch (WebException webEx)
            {
                HttpWebResponse httpResponse = webEx.Response as HttpWebResponse;
                using (Stream responseStream = httpResponse.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(responseStream))
                    {
                        ReturnValue = GetWebResponseString(httpResponse);

                        if (issave)
                        {
                            SMBPDataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method:" + webEx.Message.Replace("'", "''") + "", actionType, agentId, clientRefId, actionPostType);
                        }
                    }
                }
                ReturnValue = null;
            }
            finally
            {
                Http.Abort();
                Http = null;
                SMBPDataBase.Error("Error : Request in Post Method : " + Url + "", Url, ReqJson, "Response in Post Method : " + ReturnValue + "", "Exception in Post Method", actionType, agentId, clientRefId, actionPostType);
            }

            if (issave)
            {
                SMBPDataBase.Information(Url, "Request captured in post Method : " + Url + "", ReqJson, ReturnValue, actionType, agentId, clientRefId, actionPostType);
            }

            return ReturnValue;
        }
        public static string GetWebResponseString(HttpWebResponse myHttpWebResponse)
        {
            StringBuilder rawResponse = new StringBuilder();
            string aa = "";
            Stream responseStream = myHttpWebResponse.GetResponseStream();
            Stream streamResponse = responseStream;
            using (responseStream)
            {
                if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip") || myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                {
                    if (myHttpWebResponse.ContentEncoding.ToLower().Contains("gzip"))
                    {
                        streamResponse = new GZipStream(streamResponse, CompressionMode.Decompress);
                    }
                    else if (myHttpWebResponse.ContentEncoding.ToLower().Contains("deflate"))
                    {
                        streamResponse = new DeflateStream(streamResponse, CompressionMode.Decompress);
                    }
                    using (StreamReader streamRead = new StreamReader(streamResponse))
                    {
                        //char[] readBuffer = new char[checked((IntPtr)myHttpWebResponse.ContentLength)];

                        //for (int count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)); count > 0; count = streamRead.Read(readBuffer, 0, Convert.ToInt32(myHttpWebResponse.ContentLength)))
                        //{
                        //    rawResponse.Append(new string(readBuffer, 0, count));
                        //}
                    }
                    aa = rawResponse.ToString();
                }
                else
                {
                    aa = (new StreamReader(streamResponse)).ReadToEnd().Trim();
                }
            }
            return aa;
        }
        public static A2ZServiceCredential GetA2ZServiceCredential(string department)
        {
            A2ZServiceCredential credential = new A2ZServiceCredential();
            try
            {
                DataTable dtA2Z = LedgerDataBase.GetA2ZServiceCredential(department, "A2Z_Suvidhaa");
                if (dtA2Z != null && dtA2Z.Rows.Count > 0)
                {
                    credential.Api_Token = dtA2Z.Rows[0]["ApiToken"].ToString();
                    credential.UserId = dtA2Z.Rows[0]["UserId"].ToString();
                    credential.SecretKey = dtA2Z.Rows[0]["SecretKey"].ToString();
                    credential.PostUrl = dtA2Z.Rows[0]["PostUrl"].ToString();
                    credential.PostType = dtA2Z.Rows[0]["Method"].ToString();
                    credential.Department = dtA2Z.Rows[0]["Department"].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return credential;
        }
        public static string GetTrackId()
        {
            return "A2Z" + Guid.NewGuid().ToString().Replace("-", "").Substring(0, 15).ToUpper();
        }
        public static List<string> MakeRequestBody(string departmentName, bool paramenter)
        {
            List<string> rutStr = new List<string>();
            try
            {
                A2ZServiceCredential credential = GetA2ZServiceCredential(departmentName);
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    rutStr.Add("{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\"" + (paramenter == false ? "}" : ","));
                    rutStr.Add(credential.PostType);
                    rutStr.Add(credential.PostUrl);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return rutStr;
        }
        public static string GenrateRandomTransactionId(string prefixString, int SizeLimit, string charCombo = null)
        {
            try
            {
                char[] chars = !string.IsNullOrEmpty(charCombo) ? charCombo.ToCharArray() : "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[SizeLimit];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetBytes(data);
                }

                StringBuilder result = new StringBuilder(SizeLimit);

                if (!string.IsNullOrWhiteSpace(prefixString))
                {
                    result.Append(prefixString);
                }

                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }

                return result.ToString();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return string.Empty;
        }
    }
}
