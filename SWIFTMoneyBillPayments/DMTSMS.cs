﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class DMTSMS
    {
        public static bool SendSms(string smsUser, string smsKey, string senderId, string entityId, string templateId, string amount, string agentid, string benName, string benid, string mobno, string remt_Mobile, string type)
        {
            try
            {
                if (type.Trim().ToLower() == "dmt_transfer")
                {
                    return DMTMoneyTransferOTP(smsUser, smsKey, senderId, entityId, templateId, amount, agentid, benName, benid, mobno, remt_Mobile);
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }

            return false;
        }

        public static bool DMTMoneyTransferOTP(string smsUser, string smsKey, string senderId, string entityId, string templateId, string amount, string agentId, string benName, string benid, string mobno, string remt_Mobile)
        {
            string otp = Common_Post.GenrateRandomTransactionId(string.Empty, 6, "1234567890");
            string message = "OTP For transaction of INR " + amount + ", to " + benName + " (" + benid + ") is " + otp + ", please do not share or forward this message. If transaction not initiated by you Call on 9883577816, 8902231371. - AnupamTravelOnline.";
            StringBuilder jsonReq = new StringBuilder();
            jsonReq.Append("{");
            jsonReq.Append("\"Authorization\":{\"User\":\"" + smsUser + "\",\"Key\":\"" + smsKey + "\"},");
            jsonReq.Append("\"Data\":{");
            jsonReq.Append("\"Sender\":\"" + senderId + "\",\"Message\":\"" + message + "\",\"Flash\":0,\"ReferenceId\":\"" + Common_Post.GetTrackId() + "\",\"EntityId\":\"" + entityId + "\",");
            jsonReq.Append("\"TemplateId\":\"" + templateId + "\",\"Mobile\":[" + mobno + "]");
            jsonReq.Append("}");
            jsonReq.Append("}");

            //bool isdeleted = A2ZDataBase.InsertUpdateDataBase("delete from DMT_OTPHistory where agentid='" + agentId + "' and status=1");
            string fileds = "agentid,benid,amount,message,otp,mobno,remt_Mobile,status,request";
            string fieldValue = "'" + agentId + "','" + benid + "','" + amount + "','" + message + "','" + otp + "','" + mobno + "','" + remt_Mobile + "','1','" + jsonReq.ToString() + "'";
            int lastId = A2ZDataBase.InsertRecordToTableReturnID(fileds, fieldValue, "DMT_OTPHistory");
            if (lastId > 0)
            {
                string response = Common_Post.Post("POST", "http://nimbusit.info/api/pushsmsjson.php", jsonReq.ToString(), "DMT_OTP", agentId, Common_Post.GetTrackId(), "DMT", true);
                if (!string.IsNullOrEmpty(response))
                {
                    string query = "update DMT_OTPHistory set response='" + response + "' where id=" + lastId + " and agentid='" + agentId + "' and benid='" + benid + "' and status=1";
                    A2ZDataBase.InsertUpdateDataBase(query);
                    JObject jo_response = JObject.Parse(response);
                    var status = jo_response["Status"];
                    if (status.ToString().Trim().ToLower() == "ok")
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        public static bool CheckDMTOTPValid(string agentid, string benid, string mobno, string amount, string otp)
        {
            string getQuery = "select * from DMT_OTPHistory where agentid='" + agentid + "' and benid='" + benid + "' and remt_Mobile='" + mobno + "' and otp='" + otp + "' and amount='" + amount + "' and status=1";

            DataTable dtOtpHis = A2ZDataBase.GetRecordFromTable(getQuery);
            if (dtOtpHis != null && dtOtpHis.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }

        public static bool DisableDMTOTPValid(string agentid, string benid, string mobno, string amount, string otp)
        {
            DataTable dtOtpHis = A2ZDataBase.GetRecordFromTable("update DMT_OTPHistory set otp='', status=0 where agentid='" + agentid + "' and benid='" + benid + "' and remt_Mobile='" + mobno + "' and otp='" + otp + "' and amount='" + amount + "' and status=1");
            if (dtOtpHis != null && dtOtpHis.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
    }
}
