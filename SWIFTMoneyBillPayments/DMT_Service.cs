﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public static class DMT_Service
    {
        public static bool InsertBankDetail(string agentId)
        {
            bool bank_inserted = false;
            try
            {
                string response = string.Empty;
                A2ZServiceCredential credential = Common_Post.GetA2ZServiceCredential("Dmt_Get_Bank");
                if (!string.IsNullOrEmpty(credential.Api_Token))
                {
                    string rowRequest = "{\"api_token\":\"" + credential.Api_Token + "\",\"userId\":\"" + credential.UserId + "\",\"secretKey\":\"" + credential.SecretKey + "\"}";
                    response = Common_Post.Post(credential.PostType, credential.PostUrl, rowRequest, "Get_Bank", agentId, Common_Post.GetTrackId(), "DMT", true);

                    if (!string.IsNullOrEmpty(response))
                    {
                        List<BankRoot> bankList = JsonConvert.DeserializeObject<List<BankRoot>>(response);
                        if (bankList != null && bankList.Count > 0)
                        {
                            foreach (var bank in bankList)
                            {
                                string query = "insert into T_DMT_BankList(bankid,bankname,ifsc,status) values(" + bank.id + ",'" + bank.name + "','" + bank.ifsc + "','" + bank.status + "')";
                                bool issuccess = A2ZDataBase.InsertUpdateDataBase(query);
                                if (issuccess)
                                {
                                    bank_inserted = true;
                                }
                                else
                                {
                                    bank_inserted = false;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return bank_inserted;
        }
        public static DataTable GetBindAllBank(string type = "")
        {
            return A2ZDataBase.GetBindAllBank(type);
        }
        public static string Dmt_Mobile_Verification(string agentId, string mobile, ref string msg)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Mob_Verify", true);
                if (rowDetails.Count == 3)
                {
                    string strRequest = rowDetails[0] + "\"mobile\":" + mobile + "}";
                    response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Mobile_Verification", agentId, Common_Post.GetTrackId(), "DMT", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        msg = "success";
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static string Get_BeneficiaryDetails(string agentId, string mobile, ref string msg)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Get_Beneficiary_List", true);
                if (rowDetails.Count == 3)
                {
                    string strRequest = rowDetails[0] + "\"mobile\":" + mobile + "}";
                    response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Get_Beneficiary_List", agentId, Common_Post.GetTrackId(), "DMT", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        msg = "success";

                        JObject jo_response = JObject.Parse(response);
                        var status = jo_response["status"];
                        if (status.ToString() == "22")
                        {
                            BeneRoot beneRespo = JsonConvert.DeserializeObject<BeneRoot>(response);
                            if (beneRespo != null)
                            {
                                foreach (var bene in beneRespo.message.data)
                                {
                                    bool isUpdated = A2ZDataBase.Update_DmtBeneficiaryDetail(agentId, mobile, bene.account_number, bene.ifsc, bene.bank_name, bene.beneId, bene.name, bene.customer_number, bene.is_bank_verified, bene.status_id);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static DataTable Get_SenderBeneficiaryDetail(string agentId, string mobile)
        {
            return A2ZDataBase.Get_SenderBeneficiaryDetail(agentId, mobile);
        }
        public static bool Dmt_UpdateRemitterDetail(string agentId, string mobile, string fname, string lname, string rem_bal, string verify)
        {
            return A2ZDataBase.Dmt_UpdateRemitterAllDetail(agentId, mobile, fname, lname, rem_bal, verify);
        }
        public static string Dmt_ReCheck_Mobile_Verification(string agentId, string mobile, ref string msg)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Mob_Verify", true);
                if (rowDetails.Count == 3)
                {
                    string strRequest = rowDetails[0] + "\"mobile\":" + mobile + "}";
                    response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Mobile_Verification", agentId, Common_Post.GetTrackId(), "DMT", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        msg = "success";
                        dynamic dyResult = JObject.Parse(response);
                        string status = dyResult.status;

                        if (status == "13")
                        {
                            ReCheck_Remitter remitter_del = JsonConvert.DeserializeObject<ReCheck_Remitter>(response);
                            if (A2ZDataBase.Dmt_UpdateRemitterExistDetail(agentId, remitter_del.message.mobile, remitter_del.message.rem_bal.ToString(), remitter_del.message.verify.ToString()))
                            {
                                msg = "redirect";
                                return response;
                            }
                        }
                        else
                        {
                            string message = dyResult.message;
                            response = message;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static string Dmt_Remitter_Registration(string agentId, string fname, string lname, string mobile, int walletType, string pincode, string location, bool saveremitter, ref string msg)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Remitter_Reg", true);
                if (rowDetails.Count == 3)
                {
                    if (saveremitter)
                    {
                        int RemtID = A2ZDataBase.InsertRemitterFirstDetail(mobile, fname, lname, pincode, location, agentId);
                    }
                    string strRequest = rowDetails[0] + "\"fname\":\"" + fname + "\",\"lname\":\"" + lname + "\",\"mobile\":\"" + mobile + "\",\"walletType\":" + walletType + "}";
                    response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Remitter_Registration", agentId, Common_Post.GetTrackId(), "DMT", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        msg = "success";
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static string Dmt_Remitter_OTP_Verification(string agentId, string mobile, string otp, ref string msg)
        {
            string response = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(otp.Trim()))
                {
                    if (otp.Length == 4)
                    {
                        List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Remitter_Otp_Verify", true);
                        if (rowDetails.Count == 3)
                        {
                            string strRequest = rowDetails[0] + "\"mobile\":\"" + mobile + "\",\"otp\":\"" + otp + "\"}";
                            response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Remitter_Otp_Verify", agentId, Common_Post.GetTrackId(), "DMT", true);
                            if (!string.IsNullOrEmpty(response))
                            {
                                msg = "success";
                            }
                        }
                    }
                    else
                    {
                        msg = "error";
                        response = "You have entered wrong otp, please check and re-enter!";
                    }
                }
                else
                {
                    msg = "error";
                    response = "You have entered wrong otp, please check and re-enter!";
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static string Dmt_Add_Beneficiary(string agentId, string accountNumber, string beneName, string ifscCode, string bankName, string mobile, ref string msg)
        {
            string response = string.Empty;
            try
            {
                int last_InstVal = A2ZDataBase.Insert_Dmt_RowBeneficiaryDetail(agentId, mobile, accountNumber, ifscCode, bankName, beneName);
                if (last_InstVal > 0)
                {
                    List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Add_Beneficiary", true);
                    if (rowDetails.Count == 3)
                    {
                        string strRequest = rowDetails[0] + "\"accountNumber\":\"" + accountNumber + "\",\"beneName\":\"" + beneName + "\",\"mobile\":\"" + mobile + "\",\"ifscCode\":\"" + ifscCode + "\",\"bankName\":\"" + bankName + "\"}";
                        response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Add_Beneficiary", agentId, Common_Post.GetTrackId(), "DMT", true);
                        if (!string.IsNullOrEmpty(response))
                        {
                            msg = "success";

                            JObject jo_response = JObject.Parse(response);
                            var status = jo_response["status"];
                            if (status.ToString() == "35")
                            {
                                msg = "success";
                                var beneId = jo_response["beneId"];
                                if (A2ZDataBase.Update_DmtBeneficiaryDetail(last_InstVal, beneId.ToString()))
                                {
                                    string benmsg = string.Empty;
                                    string getBenDel = Get_BeneficiaryDetails(agentId, mobile, ref benmsg);
                                }
                            }
                            else
                            {
                                msg = "failed";
                            }
                        }
                        else
                        {
                            msg = "failed";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static DataTable GetT_RemitterDetail(string agentId, string regMobile)
        {
            return A2ZDataBase.GetT_RemitterDetail(agentId, regMobile);
        }
        public static string Dmt_DeleteBeneficiary(string beneId, string agentId, ref string msg)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Delete_Beneficiary", true);
                if (rowDetails.Count == 3)
                {
                    string strRequest = rowDetails[0] + "\"beneId\":\"" + beneId + "\"}";
                    response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Delete_Beneficiary", agentId, Common_Post.GetTrackId(), "DMT", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        //{ 'status': '37', 'message': 'OTP has been sent at remitter mobile number' }
                        JObject jo_response = JObject.Parse(response);
                        var status = jo_response["status"];
                        if (status.ToString() == "37")
                        {
                            msg = "success";
                        }
                        else
                        {
                            msg = "failed";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return response;
        }
        public static string Dmt_Confirm_DeleteBeneficiary(string beneId, string otp, string mobile, string agentId, ref string msg)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Confirm_Delete_Beneficiary", true);
                if (rowDetails.Count == 3)
                {
                    string strRequest = rowDetails[0] + "\"beneId\":\"" + beneId + "\",\"otp\":" + otp + "}";
                    response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Confirm_Delete_Beneficiary", agentId, Common_Post.GetTrackId(), "DMT", true);
                    if (!string.IsNullOrEmpty(response))
                    {
                        //{"status": 38, "message": “Beneficiary deleted successfully"}
                        JObject jo_response = JObject.Parse(response);
                        var status = jo_response["status"];
                        if (status.ToString() == "38")
                        {
                            msg = "success";
                            bool isdeleted = A2ZDataBase.Dmt_DeleteBeneficiaryDetail(agentId, mobile, beneId);
                        }
                        else
                        {
                            msg = "failed";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return response;
        }
        public static DataTable GetFundTransferVeryficationDetail(string agentId, string benId, string mobile, ref DataTable dtSenderDel)
        {
            return A2ZDataBase.GetFundTransferVeryficationDetail(agentId, benId, mobile, ref dtSenderDel);
        }
        public static string Dmt_Money_Transaction(string agentId, string beneId, string accountNumber, string mobile, string amount, int walletType, string channel, string trackid, ref string msg)
        {
            string response = string.Empty;
            try
            {
                string channelstr = (channel == "1" ? "NEFT" : "IMPS");
                int inserted_Val = A2ZDataBase.Dmt_InsertTransaction(agentId, mobile.ToString(), trackid, accountNumber, beneId, amount.ToString(), walletType.ToString(), channel.ToString(), channelstr);
                if (inserted_Val > 0)
                {
                    List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Transaction", true);
                    if (rowDetails.Count == 3)
                    {
                        string strRequest = rowDetails[0] + "\"accountNumber\":\"" + accountNumber + "\",\"mobile\":" + mobile + ",\"beneId\":\"" + beneId + "\",\"amount\":" + amount + ",\"walletType\":" + walletType + ",\"channel\":" + channel + ",\"clientId\":\"" + trackid + "\"}";
                        response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Dmt_Transaction", agentId, trackid, "DMT", true);
                        if (!string.IsNullOrEmpty(response))
                        {
                            JObject jo_response = JObject.Parse(response);
                            var status = jo_response["status"];
                            var message = jo_response["message"];
                            if (status.ToString() == "34" && message.ToString().ToLower().Contains("success"))
                            {
                                msg = "success";
                            }
                            else if (status.ToString() == "34" && message.ToString().ToLower().Contains("accepted"))
                            {
                                msg = "accepted";
                            }
                            else if (status.ToString() == "3" && message.ToString().ToLower().Contains("pending"))
                            {
                                msg = "pending";
                            }
                            else if (status.ToString() == "3" && message.ToString().ToLower().Contains("initiated"))
                            {
                                msg = "initiated";
                            }
                            else
                            {
                                msg = "failed";
                            }

                            if (msg.Contains("success") || msg.Contains("accepted") || msg.Contains("pending"))
                            {
                                var txnId = jo_response["txnId"];
                                var bankRefNo = jo_response["bankRefNo"];
                                var clientId = jo_response["clientId"];

                                bool isUpdated = A2ZDataBase.Dmt_UpdateTransaction(agentId, mobile.ToString(), txnId.ToString(), bankRefNo.ToString(), status.ToString(), msg, message.ToString(), clientId.ToString(), inserted_Val);

                                //DataTable dtSender = GetT_RemitterDetail(agentId, mobile);
                                //if (dtSender != null && dtSender.Rows.Count > 0)
                                //{
                                //    string remainingLimit = dtSender.Rows[0]["rem_bal"].ToString();
                                //    string remaingamt = (Convert.ToDouble(remainingLimit) - Convert.ToDouble(amount)).ToString();
                                //    bool isRemAmount = A2ZDataBase.Dmt_UpdateRemitterRemaingAMount(agentId, mobile.ToString(), remaingamt);
                                //}
                            }
                            bool isRemUpdated = Dmt_UpdateRemainingLimit(agentId, mobile, amount.ToString(), "debit");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                msg = "error";
                response = ex.Message;
            }
            return response;
        }
        public static DataTable Get_TransactionDetail(string agentId, string mobile, string trackId, string txnId = "")
        {
            return A2ZDataBase.Get_TransactionDetail(agentId, mobile, trackId, txnId);
        }
        public static bool Dmt_UpdateRemainingLimit(string agentId, string mobile, string amount, string type)
        {
            string remaingamt = string.Empty;
            DataTable dtSender = GetT_RemitterDetail(agentId, mobile);
            if (dtSender != null && dtSender.Rows.Count > 0)
            {
                string remainingLimit = dtSender.Rows[0]["rem_bal"].ToString();
                if (type == "debit")
                {
                    remaingamt = (Convert.ToDouble(remainingLimit) - Convert.ToDouble(amount)).ToString();
                }
                else
                {
                    remaingamt = (Convert.ToDouble(remainingLimit) + Convert.ToDouble(amount)).ToString();
                }
            }
            return A2ZDataBase.Dmt_UpdateRemitterRemaingAMount(agentId, mobile.ToString(), remaingamt);
        }
        public static DataTable Get_Dmt_TranstionHistory(string agentId, string mobile, string fromdate, string todate = "", string trackid = "", string filestatus = "")
        {
            return A2ZDataBase.Get_Dmt_TranstionHistory(agentId, mobile, fromdate, todate, trackid, filestatus);
        }
        public static string CheckTransaction_TrackId(string agentId, string mobile, string trackid)
        {
            string response = string.Empty;

            List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Check_Trans", true);
            if (rowDetails.Count == 3)
            {
                string strRequest = rowDetails[0] + "\"clientId\":\"" + trackid + "\"}";
                response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Dmt_CheckTransaction", agentId, trackid, "DMT", true);
                if (!string.IsNullOrEmpty(response))
                {
                    JObject jo_response = JObject.Parse(response);
                    var status = jo_response["status"];
                    var message = jo_response["message"];

                    if (message.ToString().ToLower().Contains("success"))
                    {
                        var description = jo_response["description"];
                        var bankRefNo = jo_response["bankRefNo"];
                        var clientId = jo_response["clientId"];
                        var txnId = jo_response["txnId"];
                        bool isUpdated = A2ZDataBase.Dmt_UpdateTransactionByTrackId(agentId, mobile.ToString(), txnId.ToString(), bankRefNo.ToString(), status.ToString(), message.ToString(), description.ToString(), clientId.ToString(), "1");
                    }
                    else if (message.ToString().ToLower().Contains("pending") || message.ToString().ToLower().Contains("inprocess") || message.ToString().ToLower().Contains("processing") || message.ToString().ToLower().Contains("initiated"))
                    {
                        var description = jo_response["description"];
                        var bankRefNo = jo_response["bankRefNo"];
                        var clientId = jo_response["clientId"];
                        var txnId = jo_response["txnId"];
                        bool isUpdated = A2ZDataBase.Dmt_UpdateTransactionByTrackId(agentId, mobile.ToString(), txnId.ToString(), bankRefNo.ToString(), status.ToString(), message.ToString(), description.ToString(), clientId.ToString(), "0");
                    }
                }
            }
            return response;
        }
        public static string Dmt_Beneficiary_Account_Verification(string agentId, string accountno, string mobile, string ifsccode, string bankname, string clientId)
        {
            string response = string.Empty;
            try
            {
                List<string> rowDetails = Common_Post.MakeRequestBody("Dmt_Account_Verify", true);
                if (rowDetails.Count == 3)
                {
                    string strRequest = rowDetails[0] + "\"accountNumber\":\"" + accountno + "\",\"ifscCode\":\"" + ifsccode + "\",\"bankName\":\"" + bankname + "\",\"mobile\":\"" + mobile + "\",\"clientId\":\"" + clientId + "\"}";
                    int inserted_Val = A2ZDataBase.Dmt_InsertTransaction(agentId, mobile.ToString(), clientId, accountno, string.Empty, "2", "0", string.Empty, string.Empty);
                    if (inserted_Val > 0)
                    {
                        response = Common_Post.Post(rowDetails[1], rowDetails[2], strRequest, "Ben_Account_Verify", agentId, clientId, "DMT", true);
                        if (!string.IsNullOrEmpty(response))
                        {
                            JObject jo_response = JObject.Parse(response);
                            var status = jo_response["status"];
                            var message = jo_response["message"];
                            if (status.ToString() == "1" || status.ToString() == "2" || status.ToString() == "3")//&& message.ToString().ToLower().Contains("success"))
                            {
                                var txnId = jo_response["txnId"];
                                var bankRefNo = jo_response["bankRefNo"];
                                var respoclientId = jo_response["clientId"];

                                bool isUpdated = A2ZDataBase.Dmt_UpdateTransaction(agentId, mobile.ToString(), txnId.ToString(), bankRefNo.ToString(), status.ToString(), message.ToString(), message.ToString(), respoclientId.ToString(), inserted_Val);
                            }

                        }
                    }
                    return response;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
            return response;
        }
    }
}
