﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SWIFTMoneyBillPayments
{
    public class BankRoot
    {
        public int id { get; set; }
        public string name { get; set; }
        public string ifsc { get; set; }
        public int status { get; set; }
    }

    public class ReCheck_Message
    {
        public string mobile { get; set; }
        public string fname { get; set; }
        public string lname { get; set; }
        public int rem_bal { get; set; }
        public int verify { get; set; }
    }

    public class ReCheck_Remitter
    {
        public int status { get; set; }
        public ReCheck_Message message { get; set; }
    }

    public class ReCheck_Errors
    {
        public List<string> mobile { get; set; }
    }

    public class ReCheck_Remitter_Error
    {
        public int status { get; set; }
        public string message { get; set; }
        public ReCheck_Errors errors { get; set; }
    }
    //--------------------Bene List-------------------------------------
    public class BeneDatum
    {
        public string account_number { get; set; }
        public string bank_name { get; set; }
        public string customer_number { get; set; }
        public string ifsc { get; set; }
        public string beneId { get; set; }
        public string name { get; set; }
        public int status_id { get; set; }
        public int is_bank_verified { get; set; }
    }

    public class BeneMessage
    {
        public List<BeneDatum> data { get; set; }
    }

    public class BeneRoot
    {
        public string status { get; set; }
        public BeneMessage message { get; set; }
    }
}
